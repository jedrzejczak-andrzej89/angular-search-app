'use strict';

// TODO: service should return data, not promise

app.controller('JobIndexController', function($scope, Job) {

    $scope.page = 1;
    $scope.loading = true;
    $scope.searchParameters = {};
    $scope.cleanedSearchParameters = {};
    $scope.jobs = {};

    $scope.loadMoreJobs = function() {
        $scope.page++;
        cleanSearchParameters();
        getJobs();
    }

    getJobs();
    getFilters();

    function getJobs() {
        $scope.loading = true;
        Job.query($scope.cleanedSearchParameters).then(function(response){
            if (response.data.page == 1) {
                $scope.jobs = response.data
            } else {
                var actualDisplayedJobs = $scope.jobs;
                $scope.jobs = response.data;
                $scope.jobs.data = actualDisplayedJobs.data.concat($scope.jobs.data);
            }
            $scope.loading = false;
        });
    }
    // TODO: refactor
    function  cleanSearchParameters() {
        var cleanedParameters = {};
        angular.forEach($scope.searchParameters, function(filterSectionValue, filterSectionKey) {
            if (angular.isObject(filterSectionValue)) {
                angular.forEach(filterSectionValue, function(checked, optionKey) {
                    if (checked && optionKey.length) {
                        if (!angular.isArray(cleanedParameters[filterSectionKey])) {
                            cleanedParameters[filterSectionKey] = [];
                        }
                        cleanedParameters[filterSectionKey].push(optionKey);
                    }
                });
            } else {
                if (filterSectionValue) {
                    cleanedParameters[filterSectionKey] = filterSectionValue;
                }
            }
        });

        cleanedParameters.page = $scope.page;

        $scope.cleanedSearchParameters = cleanedParameters;
    }

    function  getFilters() {
        Job.filters().then(function(response){
            $scope.filterSections = response.data;
        });
    }

    // TODO: add some delay + cancel previous request
    $scope.$watch('searchParameters', function (newValue, oldValue) {
        $scope.page = 1;
        cleanSearchParameters();
        getJobs();
    }, true);

});

