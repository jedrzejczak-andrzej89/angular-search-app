'use strict';

app.factory('Job', function ($http) {
    var path = 'api/jobs';

    return {
        query: function(parameters) {
            return $http.get(path + '?' + $.param(parameters));
        },
        filters: function() {
            return $http.get(path + '/filters');
        },
        show: function (id) {
            return $http.get(path + '/' + id);
        }
    };
});
