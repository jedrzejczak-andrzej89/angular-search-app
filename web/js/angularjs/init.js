'use strict';

var app = angular.module('mondiApp', ['ngResource', 'ngRoute']);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/:id', {
            templateUrl: '/js/angularjs/views/job/show.html',
            controller: 'JobShowController',
            resolve: {
                job: ['$route', 'Job', function ($route, Job) {
                    return Job.show($route.current.params.id);
                }]
            }
        }).
        otherwise({
            templateUrl: '/js/angularjs/views/job/index.html',
            controller: 'JobIndexController'
        });
    }]);