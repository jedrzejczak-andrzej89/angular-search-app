<?php

namespace Nekk\Bundle\OpinionBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OpinionAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('content', null, ['label' => 'Treść'])
            ->add('name', null, ['label' => 'Imię i nazwisko'])
            ->add('isActive', null, ['label' => 'Aktywna'])
            ->add('position', null, ['label' => 'Pozycja']);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'Imię i nazwisko'])
            ->add('position', null, ['label' => 'Pozycja'])
            ->add('isActive', null, ['label' => 'Aktywna'])
            ->add('_action', 'actions', array(
                'label' => 'Akcje',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('content', null, ['label' => "Treść", 'attr' => ['class' => 'redactor-editor']])
            ->add('name', null, ['label' => 'Imię i nazwisko'])
            ->add('category', 'choice', [
                'label' => 'Kategoria',
                'choices' => [
                    '0' => 'Opinia pracownika',
                    '1' => 'Opinia klienta',
                ]
            ])
            ->add('position', null, ['label' => 'Pozycja'])
            ->add('isActive', null, ['label' => 'Aktywna']);
    }

}
