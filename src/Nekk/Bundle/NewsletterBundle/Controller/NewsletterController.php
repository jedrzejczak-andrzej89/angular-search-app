<?php
namespace Nekk\Bundle\NewsletterBundle\Controller;

use NewsletterBundle\Entity\Newsletter;
use Nekk\Bundle\NewsletterBundle\Form\NewsletterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class NewsletterController extends Controller
{
    /**
     * @Route("/newsletter/dodaj" , options={"expose"=true})
     * @Template("newsletter:add.html.twig")
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm('newsletter_type');
        if ($request->isMethod('POST')) {
            if ($form->submit($request) && $form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                $subscriber = $form->getData();
                $em->persist($subscriber);
                $em->flush();
                $this->sendConfirmationToSubscriber($subscriber);

                if ($request->isXmlHttpRequest()) {
                    return new JsonResponse('Dziękujemy za zapisanie się do newslettera.');
                } else {
                    $this->addFlash('notice', 'Dziękujemy za zapisanie się do newslettera.');

                    return $this->redirect($this->generateUrl('portal_portal_index'));
                }
            }

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse('Formularz został niepoprwanie wypełniony.');
            }
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse('Błąd');
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route("/newsletter/anuluj/{hash}/{email}")
     */
    public function cancelAction($hash, $email)
    {
        $em = $this->getDoctrine()->getManager();

        if ($subscriber =$this->checkSubscriber($hash, $email)) {
            $em->remove($subscriber);
            $em->flush();
            $this->addFlash('notice', 'Subskrypcja newslettera została anulowana');

            return $this->redirect($this->generateUrl('portal_portal_index'));
        }

        throw $this->createNotFoundException();
    }

    /**
     * @Route("/newsletter/potwierdzenie/{hash}/{email}")
     */
    public function confirmAction($hash, $email)
    {
        $em = $this->getDoctrine()->getManager();

        if ($subscriber =$this->checkSubscriber($hash, $email)) {
            $subscriber->setConfirmEmail(true);
            $em->persist($subscriber);
            $em->flush();

            $this->addFlash('notice', 'Subskrypcja newslettera została potwierdzona');

            return $this->redirect($this->generateUrl('portal_portal_index'));
        }

        throw $this->createNotFoundException();
    }

    private function checkSubscriber($hash, $email)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Newsletter $subscriber */
        $subscriber = $em->getRepository('NewsletterBundle:Newsletter')->findOneByEmail($email);

        return ($subscriber && $hash == $subscriber->getHash()) ? $subscriber : false;
    }

    private function sendConfirmationToSubscriber(Newsletter $subscriber)
    {
        $email = $this->container->getParameter('send_email_address');
        $name = $this->container->getParameter('send_email_name');
        $theme = $this->container->getParameter('theme');

        $message = \Swift_Message::newInstance()
            ->setSubject("Potwierdź subskrybcje newslettera")
            ->setFrom([$email => $name])
            ->setTo($subscriber->getEmail())
            ->setBody($this->renderView(sprintf(':%s/newsletter:confirmationEmail.html.twig', $theme),
                [
                    'subscriber' => $subscriber
                ]), 'text/html');

        $this->get('mailer')->send($message);
    }
}
