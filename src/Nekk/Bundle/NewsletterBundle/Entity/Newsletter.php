<?php

namespace Nekk\Bundle\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="newsletter_newsletter")
 * @ORM\Entity()
 * @UniqueEntity(fields = "email", message = "Ten adres posiada już subskrybcję")
 */
class Newsletter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\Email(message = "Nieprawidłowy adres email")
     * @ORM\Column(name="email", type="string", unique=true, length=255)
     */
    private $email;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $agreementToReceiveMessages;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $processingOfPersonalData;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $confirmEmail = false;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $email
     * @return Newsletter
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function getHash(){
        return self::getEmailHash($this->getId(), $this->getEmail());
    }

    public static function getEmailHash($id, $email){
        return substr(md5("qwedsadbgyh {$id} mdjdashygvyu324nds {$email})"), 0, 8);
    }

    /**
     * @param boolean $confirmEmail
     * @return Newsletter
     */
    public function setConfirmEmail($confirmEmail)
    {
        $this->confirmEmail = $confirmEmail;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getConfirmEmail()
    {
        return $this->confirmEmail;
    }

    /**
     * @return boolean
     */
    public function isAgreementToReceiveMessages()
    {
        return $this->agreementToReceiveMessages;
    }

    /**
     * @param boolean $agreementToReceiveMessages
     */
    public function setAgreementToReceiveMessages($agreementToReceiveMessages)
    {
        $this->agreementToReceiveMessages = $agreementToReceiveMessages;
    }

    /**
     * @return boolean
     */
    public function isProcessingOfPersonalData()
    {
        return $this->processingOfPersonalData;
    }

    /**
     * @param boolean $processingOfPersonalData
     */
    public function setProcessingOfPersonalData($processingOfPersonalData)
    {
        $this->processingOfPersonalData = $processingOfPersonalData;
    }
}
