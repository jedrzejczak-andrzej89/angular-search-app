<?php

namespace Nekk\Bundle\NewsletterBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\NotNull;

class NewsletterType extends AbstractType
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        list($agreementToReceiveMessages, $processingOfPersonalData) = $this->getAgreements();

        $builder
            ->add('email', 'email', [
                'attr' => ['placeholder' => 'Wpisz swój adres e-mail'],
                'constraints' => [
                    new NotNull(['message' => 'Pole e-mail jest wymagane.'])
                ]
            ])
            ->add('agreementToReceiveMessages', 'checkbox', [
                'required' => true,
                'label' => $agreementToReceiveMessages,
                'constraints' => [
                    new IsTrue(['message' => 'Zgoda na otrzymywanie wiadomości jest wymagana.'])
                ]
            ])
            ->add('processingOfPersonalData', 'checkbox', [
                'required' => true,
                'label' => $processingOfPersonalData,
                'constraints' => [
                    new IsTrue(['message' => 'Zgoda na przetwarzanie danych osobowych jest wymagana.'])
                ]
            ])
            ;
    }

    private function getAgreements()
    {
        $companyName = $this->container->getParameter('company_full_name');
        $companyWebsite = $this->container->getParameter('company_website');
        $messagesAgreement = sprintf('Wyrażam zgodę na otrzymywanie od %s informacji handlowej w rozumieniu art. 10 ust. 2 Ustawy z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną (Dz.U. Nr 144, poz. 1204) na podany przy rejestracji adres poczty elektronicznej.',
                ($companyName) ? $companyName : 'NEKK sp. z o.o.'
            );
        $processingAgreement = sprintf('Poprzez zapianie się do Newslettera serwisu nekk.pl wyrażam zgodę na przetwarzanie moich danych osobowych w celu wykonania zobowiązań oraz w celach marketingowych zgodnie z ustawą z 29 sierpnia 1997 r. o ochronie danych osobowych.',
            ($companyWebsite) ? $companyWebsite : 'nekk.pl'
        );

        return [$messagesAgreement, $processingAgreement];
    }

    public function getName()
    {
        return 'newsletter_type';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Nekk\Bundle\NewsletterBundle\Entity\Newsletter'
        ]);
    }
}