<?php

namespace Nekk\Bundle\CmsBundle\Controller;

use Nekk\Bundle\FrontendBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nekk\Bundle\CmsBundle\Entity\CMS;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends BaseController
{
    // TODO: do it better
    // implement some service
    // service should:
    // call controller takes into account page profile
    // set current page depends on current url
    // set languages (later)
    // add controller & view fields to cms entity
    //
    // http://symfony.com/doc/current/book/controller.html#forwarding-to-another-controller

    /*
     * @1111Route("/{slug}", name="default_page")
     * @1111Method({"GET"})
     * @1111ParamConverter("page", class="CMSBundle:CMS", options={"slug" = "slug"})
     */

    /**
     * @Route("/", name="home")
     * @Method({"GET"})
     */
    public function homeAction()
    {
        $view = 'CmsBundle/home.html.twig';
        $cms = $this->getDoctrine()->getRepository('NekkCmsBundle:CMS')->findOneBySlug('home');

        if ($cms) {
            return $this->render($view, compact('cms'));
        }

        throw new NotFoundHttpException();
    }

    /**
     * @Route("/{slug}", name="default_page")
     * @Method({"GET"})
     */
    public function showAction($slug)
    {
        $view = 'CmsBundle/page.html.twig';

        if (!$slug) {
            $slug == 'home';
        } elseif ($slug == 'szukaj-pracy-wyszukiwarka') {
            $view = 'JobBundle/index.html.twig';
        }

        $cms = $this->getDoctrine()->getRepository('NekkCmsBundle:CMS')->findOneBySlug($slug);

        if ($cms) {
            return $this->render($view, compact('cms'));
        }

        throw new NotFoundHttpException();
    }
}

