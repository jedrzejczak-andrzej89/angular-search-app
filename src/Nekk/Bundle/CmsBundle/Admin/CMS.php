<?php
namespace Nekk\Bundle\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CMS extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', ['class' => 'col-md-8'])
            ->add('title', null, ['label' => "Tytuł"])
            ->add('slug', null, [
                'help' => 'Ustaw tylko jeśli potrzebujesz',
                'label' => 'Slug',
                'required' => false
                ])
            ->add('content', null, ['label' => "Treść", 'attr' => ['class' => 'redactor-editor']])
            ->add('gallery', null, ["query_builder" => $this->getGalleriesQb(), 'label' => "Galeria"])
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, ['label' => 'Tytuł'])
            ->add('slug', null, ['label' => "Nazwa w URL"]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, ['label' => "Tytuł"])
            ->add('slug', null, ['label' => 'Slug']);
    }

    private function getGalleriesQb()
    {
        // TODO: should be in repository
        return $this->modelManager
            ->getEntityManager('Application\Sonata\MediaBundle\Entity\Gallery')
            ->getRepository('Application\Sonata\MediaBundle\Entity\Gallery')
//            ->getCMSGalleriesQb();
            ->createQueryBuilder('g');
    }
}

