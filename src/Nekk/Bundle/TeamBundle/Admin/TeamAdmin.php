<?php

namespace Nekk\Bundle\TeamBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TeamAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, ['label' => 'Imię'])
            ->add('surname', null, ['label' => 'Nazwisko'])
            ->add('phone', null, ['label' => 'Telefon'])
            ->add('cellPhone', null, ['label' => 'Telefon komórkowy'])
            ->add('email', null, ['label' => 'Email'])
            ->add('position', null, ['label' => 'Pozycja'])
            ->add('isActive', null, ['label' => 'Aktywna'])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'Imię'])
            ->add('surname', null, ['label' => 'Nazwisko'])
            ->add('phone', null, ['label' => 'Telefon'])
            ->add('cellPhone', null, ['label' => 'Telefon komórkowy'])
            ->add('email', null, ['label' => 'Email'])
            ->add('position', null, ['label' => 'Pozycja'])
            ->add('isActive', null, ['label' => 'Aktywna'])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
//        $image = $this->getSubject()->getTeamPicture();
//        dump($this->getRequest());
//        die();

        $formMapper
            ->add('name', null, ['label' => 'Imię'])
            ->add('surname', null, ['label' => 'Nazwisko'])
            ->add('phone', null, ['label' => 'Telefon'])
            ->add('cellPhone', null, ['label' => 'Telefon komórkowy'])
            ->add('email', null, ['label' => 'Email'])
            ->add('contentMedia', 'sonata_type_model_list', ["required" => false, 'label' => "Obrazek"])
            ->add('position', null, ['label' => 'Pozycja'])
            ->add('isActive', null, ['label' => 'Aktywna'])
            ;



    }

}
