<?php

namespace Nekk\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    public function render($view, array $parameters = array(), Response $response = null)
    {
        $theme = $this->container->getParameter('theme');
        $view = sprintf(':%s:%s', $theme, $view);
        return $this->container->get('templating')->renderResponse($view, $parameters, $response);
    }
}
