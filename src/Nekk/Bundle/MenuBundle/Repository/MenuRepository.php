<?php
namespace Nekk\Bundle\MenuBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class MenuRepository extends NestedTreeRepository
{
    public function getMenu($logged = false)
    {
         $qb = $this->createQueryBuilder('c')
            ->where('c.active = true');

        if (!$logged) {
            $qb->andWhere('c.forMembers = false');
        }

        return $qb->orderBy('c.root, c.lft', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }
}