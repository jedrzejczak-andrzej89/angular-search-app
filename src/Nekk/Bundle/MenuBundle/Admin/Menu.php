<?php
namespace Nekk\Bundle\MenuBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Validator\ErrorElement;
use MenuBundle\Entity\Menu as Entity;

class Menu extends Admin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, ['label' => 'Nazwa']);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Element')
                ->add('name', null, ['label' => 'Nazwa'])
                ->add('parent', null, ['label' => 'Element nadrzędny'])
                ->add('position', null, ['label' => 'Pozycja'])
                ->add('active', null, ['label' => 'Aktywny'])
                ->add('visible', null, ['label' => 'Widoczny w menu'])
                ->add('forMembers', null, ['label' => 'Widoczny tylko dla zalogowanych'])
            ->end()
            ->with('Odnośnik', [
                'description' => 'Wybierz podstronę lub kategorię'
            ])
                ->add('cms', 'entity', [
                    'class'    => 'NekkCmsBundle:CMS',
                    'property' => 'title',
                    'label'    => 'Podstrona',
                    'multiple' => false,
                    'required' => false
                ])
                ->add('route', 'entity', [
                    'class'    => 'MenuBundle:Route',
                    'property' => 'name',
                    'label'    => 'Odnośnik do akcji',
                    'multiple' => false,
                    'required' => false
                ])
                ->add('url', null, ['label' => 'Link'])
            ->end();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, ['label' => 'Id'])
            ->addIdentifier('name', null, ['label' => 'Nazwa'])
            ->addIdentifier('parent', null, ['label' => 'Element nadrzędny'])
            ->add('position', null, ['label' => 'Pozycja']);
    }

    public function validate(ErrorElement $error, $object)
    {
        /** @var Entity $object */
        $cms = $object->getCms();
        $url = $object->getUrl();
        $route = $object->getRoute();

        $elements = [$cms, $url, $route];


        if (!array_filter($elements)) {
            $error->with('cms')->addViolation('Zaznacz jeden z elementów.')->end();
        } else {
            if (!$this->validMenuValues($elements)) {
                $error->with('cms')->addViolation('Zaznacz tylko jeden element.')->end();
            }
        }
    }

    private function validMenuValues($elements)
    {
        $notNull = false;
        foreach ($elements as $el) {
            if ($el) {
                if ($notNull) {
                    return false;
                } else {
                    $notNull = true;
                }
            }
        }

        return true;
    }
}