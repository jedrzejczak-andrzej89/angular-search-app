<?php

namespace Nekk\Bundle\MenuBundle\Block;

use Nekk\Bundle\CmsBundle\Entity\CMS;
use CoreBundle\Block\AbstractBlock;
use Doctrine\ORM\EntityManager;
use Nekk\Bundle\MenuBundle\Entity\Menu;
use Nekk\Bundle\MenuBundle\Entity\Route;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class MenuBlock extends AbstractBlock
{
    const CACHE_TIME = 0;

    private $em;

    private $router;

    private $authorizationChecker;

    public function __construct(
        $name,
        EngineInterface $templating,
        ContainerInterface $container,
        EntityManager $em,
        Router $router,
        AuthorizationChecker $authorizationChecker
    )
    {
        parent::__construct($name, $templating, $container);
        $this->em = $em;
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'id' => null,
            'template' => 'MenuBundle/default.html.twig',
            'cms' => null,
        ]);
    }

    public function getCacheKeys(BlockInterface $block)
    {
        return [
            'type' => $this->name
        ];
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $menuRepository = $this->em->getRepository('Nekk\Bundle\MenuBundle\Entity\Menu');
        $settings = $blockContext->getSettings();

        ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) ? $logged = true : $logged = false;

        $menu = $menuRepository->getMenu($logged);
        $menu = $this->setUrlForMenuElements($menu);
        $menu = $this->setSelectedForMenuElement($menu, $settings['cms']);

        $menuTree = $menuRepository->buildTree($menu);
        $menuTree = $this->setSelectedForMenuTree($menuTree);

        usort($menuTree, function ($a, $b) {
            return $a['position'] == $b['position'] ?
                $a['slug'] > $b['slug'] : $a['position'] > $b['position'];
        });


        return $this->renderResponse($blockContext->getTemplate(), compact('menuTree'), $response)
            ->setTtl(self::CACHE_TIME);
    }

    private function setUrlForMenuElements($menu)
    {
        /**
         * @var Menu $m
         */
        foreach ($menu as $k => $m) {
            /** @var Menu $menuItem */
            $menuItem = $this->em->getRepository('Nekk\Bundle\MenuBundle\Entity\Menu')->findOneById($m['id']);
            $link = null;
            /** @var CMS $cms */
            $cms = $menuItem->getCms();
            /** @var Route $route */
            $route = $menuItem->getRoute();

            if ($cms) {
                $link = $this->router->generate('default_page', ['slug' => $cms->getSlug()]);
            } elseif ($route) {
                $link = $this->router->generate($route->getRoute());
            } else {
                $link = $menuItem->getUrl();
            }

            $menu[$k]['link'] = $link;
        }

        return $menu;
    }

    private function setSelectedForMenuElement($menu, CMS $selectedMenuItem)
    {
        /**
         * @var Menu $m
         */
        foreach ($menu as $k => $m) {
            /** @var Menu $menuItem */
            $menuItem = $this->em->getRepository('Nekk\Bundle\MenuBundle\Entity\Menu')->findOneById($m['id']);
            $link = null;
            /** @var CMS $cms */
            $cms = $menuItem->getCms();

            $menu[$k]['__selected'] = ($cms->getId() == $selectedMenuItem->getId());
            $menu[$k]['__currentCms'] = ($cms->getId() == $selectedMenuItem->getId());
        }

        return $menu;
    }

    private function setSelectedForMenuTree($menuTree)
    {
        foreach ($menuTree as &$menuItem) {
            $menuItem['__selected'] = $this->setSelectedForMenuTreeItem($menuItem);
        }
        return $menuTree;
    }

    private function setSelectedForMenuTreeItem(&$menuItem)
    {
        foreach ($menuItem['__children'] as &$menuSubItem) {
            if ($this->setSelectedForMenuTreeItem($menuSubItem)) {
                $menuItem['__selected'] = true;
            }
        }
        return $menuItem['__selected'];
    }
}