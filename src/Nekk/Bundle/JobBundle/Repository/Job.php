<?php

namespace Nekk\Bundle\JobBundle\Repository;

class Job extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param array $parameters
     * @param int $page
     * @param $limit
     * @return array $jobs
     *          'total' => int Defines total number of jobs take into account filters
     *          'page' => int Defines actual returned page
     *          'availablePages' => int Defines total numbers of pages take into account filters
     *          'data' => jobs after serializer
     */
    public function search(array $parameters = [], $page = 1, $limit)
    {
        $queryBulider = $this->createQueryBuilder('job')
            ->where('job.expiryDate >= :expiryDate');
        $parameters['expiryDate'] = date('Y-m-d H:i:s');

        if (isset($parameters['categories'])) {
            $queryBulider->leftJoin('job.categories', 'category')
                ->andWhere('category.id IN (:categories)');
        }

        if (isset($parameters['locations'])) {
            $queryBulider->leftJoin('job.regions', 'region')
                ->andWhere('region.id IN (:locations)');
        }

        if (isset($parameters['offersOfThePeriod'])) {
            $queryBulider->andWhere('job.updateDate > :offersOfThePeriod');
        }

        $queryBulider->orderBy('job.updateDate', 'DESC');
        $queryBulider->setParameters($parameters);

        $total = $this->getTotalFromQueryBuilder($queryBulider);

        return [
            'total' => $total,
            'page' => $page,
            'availablePages' => ceil($total / $limit),
            'data' => $this->getResultFromQueryBuilder($queryBulider, $page, $limit),
        ];
    }

    /**
     * @param $queryBulider
     * @param $page
     * @param $limit
     * @return mixed
     */
    private function getResultFromQueryBuilder($queryBulider, $page, $limit)
    {
        $qb = clone $queryBulider;

        return $qb->groupBy('job.id')
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $queryBulider
     * @return int
     */
    private function getTotalFromQueryBuilder($queryBulider)
    {
        $qb = clone $queryBulider;

        $qb->select('COUNT(DISTINCT(job))');

        return (int) $qb->getQuery()
            ->getSingleScalarResult();
    }
}
