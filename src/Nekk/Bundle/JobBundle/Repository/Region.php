<?php
/**
 * Created by PhpStorm.
 * User: ajedrzejczak
 * Date: 29.02.16
 * Time: 14:33
 */

namespace Nekk\Bundle\JobBundle\Repository;

class Region extends \Doctrine\ORM\EntityRepository
{
    public function getPolandIds()
    {
        $ids = $this->createQueryBuilder('region')
            ->select('region.id')
            ->where('region.id != 1')
            ->getQuery()
            ->getArrayResult();

        return array_column($ids, 'id');
    }

    public function getGermanIds()
    {
        $ids = $this->createQueryBuilder('region')
            ->select('region.id')
            ->where('region.id = 1')
            ->getQuery()
            ->getArrayResult();

        return array_column($ids, 'id');
    }
}