<?php

namespace Nekk\Bundle\JobBundle\Repository;

class Category extends \Doctrine\ORM\EntityRepository
{
    public function getAllActiveCategories()
    {
        return $this->createQueryBuilder('category')
            ->select('category.id AS key, category.name AS value')
            ->getQuery()
            ->getArrayResult();
    }
}
