<?php

namespace Nekk\Bundle\JobBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use Nekk\Bundle\FrontendBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nekk\Bundle\JobBundle\Entity\Job;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api/jobs")
 */
class JobController extends BaseController
{
    /**
     * @Route("", name="job_api_jobs")
     * @Method({"GET"})
     * @View()
     */
    public function indexAction(Request $request)
    {
        // TODO: inject service
        $jobSearch = $this->get('nekk.job.search');
        $jobSearch->setParameters($request->query->all());
        return $jobSearch->search();
    }

    /**
     * @Route("/synchronize", name="job_api_jobs_synchronize")
     * @Method({"GET"})
     * @View()
     */
    public function synchronizeAction()
    {
        // TODO: inject service
        $jobProvider = $this->get('nekk.job.provider');
        return $jobProvider->import();
    }

    /**
     * @Route("/filters", name="job_api_jobs_filters")
     * @Method({"GET"})
     * @View()
     */
    public function filtersAction()
    {
        // TODO: inject service
        $jobSearch = $this->get('nekk.job.search');
        return [
            'locations' => [
                'key' => 'locations',
                'title' => 'Lokalizacja',
                'control' => 'checkbox',
                'options' => $jobSearch->getLocations(),
            ],
            /*[
                'key' => 'formOfEmployment',
                'title' => 'Forma zatrudnienia',
                'options' => $jobSearch->getFormOfEmployment(),
            ],
            */
            'categories' => [
                'key' => 'categories',
                'title' => 'Branża',
                'control' => 'checkbox',
                'options' => $jobSearch->getCategories(),
            ],
            'offersOfThePeriod' => [
                'key' => 'offersOfThePeriod',
                'title' => 'Oferty z okresu',
                'control' => 'radio',
                'options' => $jobSearch->getOffersOfThePeriod(),
            ],
        ];
    }

    /**
     * @Route("/{id}", name="job_api_job_show")
     * @Method({"GET"})
     * @View()
     * @ParamConverter("job", class="JobBundle:Job")
     */
    public function showAction(Job $job)
    {
        return $job;
    }
}
