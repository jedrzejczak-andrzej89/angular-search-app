<?php
/**
 * Created by PhpStorm.
 * User: ajedrzejczak
 * Date: 24.02.16
 * Time: 08:32
 */

namespace Nekk\Bundle\JobBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobSearch
{
    private $container;
    private $entityManager;

    static $allowedParameters = ['locations', 'formOfEmployment', 'categories', 'offersOfThePeriod', 'page'];
    private $rawParameters;
    private $parameters = [];

    static $jobsOnPage = 2;
    private $page;

    public function __construct(ContainerInterface $container, EntityManager $entityManager)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    public function setParameters($parameters = [])
    {
        $this->rawParameters = $parameters;
        $this->initParameters();
    }

    public function getLocations()
    {
        return [
            [
                'key' => '',
                'value' => 'wszędzie',
            ],
            [
                'key' => 'pl',
                'value' => 'Polska',
            ],
            [
                'key' => 'de',
                'value' => 'Niemcy',
            ],
        ];
    }

    public function getFormOfEmployment()
    {
        // TODO: wait for client
        return [
            '-' => 'wszystkie',
            'stala' => 'praca stała',
            'tymczasowa' => 'praca tymczasowa',
        ];
    }

    public function getCategories()
    {
        return $this->entityManager->getRepository('JobBundle:Category')
            ->getAllActiveCategories();
    }

    public function getOffersOfThePeriod()
    {
        return [
            [
                'key' => '',
                'value' => 'wszystkie',
            ],
            [
                'key' => '1d',
                'value' => '24h',
            ],
            [
                'key' => '3d',
                'value' => '3 dni',
            ],
            [
                'key' => '7d',
                'value' => '7 dni',
            ],
            [
                'key' => '14d',
                'value' => '14 dni',
            ],
            [
                'key' => '30d',
                'value' => '30 dni',
            ],
        ];
    }

    /**
     * @return array
     */
    public function search()
    {
        return $this->entityManager->getRepository('JobBundle:Job')
                    ->search(
                        $this->parameters,
                        $this->getPage(),
                        self::$jobsOnPage
                    );
    }

    // TODO: move to another class
    private function initParameters()
    {
        foreach (self::$allowedParameters as $allowedParameter) {
            if (isset($this->rawParameters[$allowedParameter]) && $this->rawParameters[$allowedParameter]) {
                $initMethodName = 'init' . ucfirst($allowedParameter);
                $this->$initMethodName($this->rawParameters[$allowedParameter]);
            }
        }
    }

    private function initLocations($rawValue)
    {
        $regionIds = [];
        if (in_array('de', $rawValue)) {
            $regionIds[] = array_merge(
                $regionIds,
                $this->entityManager->getRepository('JobBundle:Region')->getGermanIds()
            );
        }
        if (in_array('pl', $rawValue)) {
            $regionIds[] = array_merge(
                $regionIds,
                $this->entityManager->getRepository('JobBundle:Region')->getPolandIds()
            );
        }

        $this->parameters['locations'] = $regionIds;
    }

    private function initFormOfEmployment()
    {
        // TODO:
    }

    private function initCategories($rawValue)
    {
        // TODO: only exists id?
        $this->parameters['categories'] = $rawValue;
    }

    private function initOffersOfThePeriod($rawValue)
    {
        // TODO: parse $rawValue
        // now work only for days
        $this->parameters['offersOfThePeriod'] = (new \DateTime())->modify('-' . $rawValue . 'ay')->format('Y-m-d');
    }

    private function initPage($rawValue)
    {
        $this->page = intval($rawValue);
    }

    private function getPage()
    {
        return $this->page ?: 1;
    }
}