<?php
namespace Nekk\Bundle\JobBundle\Service\Provider\EsCandidate;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Nekk\Bundle\JobBundle\Entity\Category;
use Nekk\Bundle\JobBundle\Entity\Job;
use Nekk\Bundle\JobBundle\Entity\Region;

abstract class Base
{
    protected $container;
    protected $entityManager;

    protected $jobsToImport;
    protected $importUrl;

    const JOB_LAST_UPDATE_TIMESTAMP = 'nekk.job.provider.escandidate.lastUpdateTimestamp';

    // TODO: setting interface
    public function __construct(ContainerInterface $container, EntityManager $entityManager, $setting)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->setting = $setting;

        $this->initImportUrl();
    }

    protected function initImportUrl()
    {
        $urlSchema = $this->container->getParameter('nekk.job.provider.escandidate.url_schema');

        $patterns = [
            '|\(:companySlug:\)|',
            '|\(:companyId:\)|',
            '|\(:lastUpdateTimestamp:\)|',
            '|\(:format:\)|',
        ];
        $replacements = [
            $this->container->getParameter('nekk.job.provider.escandidate.company_slug'),
            $this->container->getParameter('nekk.job.provider.escandidate.company_id'),
            $this->setting->get(self::JOB_LAST_UPDATE_TIMESTAMP, 0),
            $this->getFormat(),
        ];

        $this->importUrl = preg_replace($patterns, $replacements, $urlSchema);
    }

    protected function synchronizeJob($job)
    {
        if ($this->isNewJob($job)) {
            $dbJob = new Job();
            $dbJob->setSourceId($job->id);
        } else {
            $dbJob = $this->entityManager->getRepository('Nekk\Bundle\JobBundle\Entity\Job')
                ->findOneBy(['sourceId' => $job->id]);
        }

        $dbJob->setTitle($job->title);
        $dbJob->setHeader($job->header);
        $dbJob->setDescription($job->description);
        $dbJob->setRequirements($job->requirements);
        $dbJob->setOffers($job->offers);
        $dbJob->setInfo($job->info);
        $dbJob->setVacancies($job->vacancies);
        $dbJob->setAddDate(new \DateTime($job->add_date));
        $dbJob->setExpiryDate(new \DateTime($job->expiry_date));
        $dbJob->setUpdateDate(new \DateTime($job->update_date));
        $dbJob->setDescriptionUrl($job->description_url);
        $dbJob->setFormUrl($job->form_url);

        $this->entityManager->persist($dbJob);

        foreach ($job->categories as $category) {
            $this->synchronizeJobCategory($dbJob, $category);
        }

        foreach ($job->regions as $region) {
            $this->synchronizeJobRegion($dbJob, $region);
        }

        $this->entityManager->flush();
    }

    protected function isNewJob($job)
    {
        return (bool) !$this->entityManager->getRepository('Nekk\Bundle\JobBundle\Entity\Job')
            ->findOneBy(['sourceId' => $job->id]);
    }

    protected function synchronizeJobCategory($dbJob, $category)
    {
        $category = $category->category;
        if ($this->isNewCategory($category)) {
            $dbCategory = new Category();
            $dbCategory->setSourceId($category->id);
        } else {
            $dbCategory = $this->entityManager->getRepository('Nekk\Bundle\JobBundle\Entity\Category')
                ->findOneBy(['sourceId' => $category->id]);
        }

        $dbCategory->setName($category->name);
        $this->entityManager->persist($dbCategory);

        $dbJob->addCategory($dbCategory);
    }

    protected function synchronizeJobRegion($dbJob, $region)
    {
        $region = $region->region;
        if ($this->isNewRegion($region)) {
            $dbRegion = new Region();
            $dbRegion->setSourceId($region->id);
        } else {
            $dbRegion = $this->entityManager->getRepository('Nekk\Bundle\JobBundle\Entity\Region')
                ->findOneBy(['sourceId' => $region->id]);
        }

        $dbRegion->setName($region->name);
        $dbRegion->setCities($region->cities);
        $this->entityManager->persist($dbRegion);

        $dbJob->addRegion($dbRegion);
    }

    protected function isNewCategory($category)
    {
        return (bool) !$this->entityManager->getRepository('Nekk\Bundle\JobBundle\Entity\Category')
            ->findOneBy(['sourceId' => $category->id]);
    }

    protected function isNewRegion($region)
    {
        return (bool) !$this->entityManager->getRepository('Nekk\Bundle\JobBundle\Entity\Region')
            ->findOneBy(['sourceId' => $region->id]);
    }
}