<?php

namespace Nekk\Bundle\JobBundle\Service\Provider\EsCandidate;

use Nekk\Bundle\JobBundle\Service\Provider\IProvider;
use Nekk\Bundle\JobBundle\Service\Provider\JsonProvider;

class Json extends Base implements IProvider
{
    use JsonProvider;

    public function import()
    {
        $this->initImport();
        foreach ($this->jobsToImport as $job) {
            $job = $job->job;
            $this->synchronizeJob($job);
        }

        return [
            'status' => 'ok',
            'updatedCount' => count($this->jobsToImport),
        ];
    }

    protected function initImport()
    {
        $rawData = $this->parse(file_get_contents($this->importUrl));
        $this->jobsToImport = $rawData->jobs;

        $this->setting->set(self::JOB_LAST_UPDATE_TIMESTAMP, $rawData->meta->code);
    }


}