<?php

namespace Nekk\Bundle\JobBundle\Service\Provider;

trait JsonProvider
{
    protected function getFormat()
    {
        return 'json';
    }

    protected function parse($data)
    {
        return json_decode($data);
    }
}