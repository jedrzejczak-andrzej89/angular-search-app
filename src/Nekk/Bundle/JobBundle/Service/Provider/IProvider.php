<?php
namespace Nekk\Bundle\JobBundle\Service\Provider;

interface IProvider
{
    public function import();
}