<?php
namespace Nekk\Bundle\SettingBundle\Service;

interface ISetting
{
    public function get($key, $defaultValue = '');
    public function set($key, $value, $force = true);
}