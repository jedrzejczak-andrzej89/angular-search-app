<?php
namespace Nekk\Bundle\SettingBundle\Service;

use Doctrine\ORM\EntityManager;
use Nekk\Bundle\SettingBundle\Entity\Setting;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Database implements ISetting
{
    protected $container;
    protected $entityManager;

    public function __construct(ContainerInterface $container, EntityManager $entityManager)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    public function get($key, $defaultValue = '')
    {
        $setting = $this->entityManager->getRepository('Nekk\Bundle\SettingBundle\Entity\Setting')
            ->findOneBySettingKey($key);

        if ($setting) {
            return $setting->getSettingValue();
        } else {
            return $defaultValue;
        }
    }

    public function set($key, $value, $force = true)
    {
        $setting = $this->entityManager->getRepository('Nekk\Bundle\SettingBundle\Entity\Setting')
            ->findOneBySettingKey($key);

        if (!$setting) {
            $setting = new Setting();
            $setting->setSettingKey($key);
        } elseif (!$force) {
             throw new \Exception('Can\'t rewrite setting ' . $key);
        }

        $setting->setSettingValue($value);
        $this->entityManager->persist($setting);
        $this->entityManager->flush();
    }
}