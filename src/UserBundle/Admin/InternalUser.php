<?php

namespace UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;

class InternalUser extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', ['class' => 'col-md-8'])
            ->add('sap')
            ->add('communicator', null, ['label' => 'Komunikator'])
            ->add('replanishment', null, ['label' => 'Replanishment'])
            ->add('replanishment_delay', null, ['label' => 'Repalnishment delay'])
            ->add('automat', null, ['label' => 'Automat'])
            ->add('fit_level', null, ['label' => 'Fit level'])
            ->add('fus', null, ['label' => 'Fus'])
            ->end()
            ;
    }
}