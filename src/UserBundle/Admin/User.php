<?php

namespace UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\UserBundle\Model\UserInterface;

use FOS\UserBundle\Model\UserManagerInterface;

class User extends Admin
{
    private $userManager;

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->addIdentifier('slug', null, ['label' => 'Nazwa w URL'])
            ->add('email')
            ->add('groups')
            ->add('enabled', null, array('editable' => true))
            ->add('locked', null, array('editable' => true))
            ->add('lastActivity', null, ['label' => 'ostatnio online'])
            ->add('createdAt');
        if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            $listMapper
                ->add('impersonating', 'string', ['template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig']);
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', ['class' => 'col-md-8'])
                ->add('username')
                ->add('slug', null, ['label' => 'Nazwa w URL'])
                ->add('email')
                ->add('plainPassword', 'text', ['required' => false])
                ->add('type', null, ['label' => 'Użytkownik wewnętrzny'])
            ->end()
            ->with('Groups', ['class' => 'col-md-4'])
                ->add('groups', 'sonata_type_model', ['required' => false, 'expanded' => true, 'multiple' => true, 'query' => $this->getGroupsQuery()])
            ->end()
            ->with('Użytkownik wewnętrzny', ['class' => 'col-md-8'])
                ->add('internalUser', 'sonata_type_admin', [
                    'label' => false,
                    'delete' => false,
                    'btn_add' => false
                ])
            ->end()
        ;

        if (!$this->getSubject()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->with('Zarządzanie kontem', ['class' => 'col-md-4'])
                ->add('locked', null, ['required' => false, 'label' => 'Konto zablokowane'])
                ->add('expired', null, ['required' => false, 'label' => 'Konto wygasło'])
                ->add('enabled', null, ['required' => false, 'label' => 'Konto aktywne'])
                ->end()
            ;
        }
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    private function getGroupsQuery()
    {
        return $this->modelManager
            ->getEntityManager('UserBundle:Group')
            ->createQueryBuilder()
            ->add('select', 'g')
            ->add('from', 'UserBundle:Group g')
            ->add('orderBy', 'g.name ASC');
    }
}