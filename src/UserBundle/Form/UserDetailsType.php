<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotNull;

class UserDetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, [
                'label' => 'Imię',
                'constraints' => [
                    new NotNull(['message' => 'Wypełnij pole'])
                ]
            ])
            ->add('lastName', null, [
                'label' => 'Nazwisko',
                'constraints' => [
                    new NotNull(['message' => 'Wypełnij pole'])
                ]
            ])
            ;
    }

    public function getName()
    {
        return 'user_details_type';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'UserBundle\Entity\User'
        ]);
    }
}