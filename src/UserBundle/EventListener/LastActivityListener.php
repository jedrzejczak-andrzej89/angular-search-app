<?php

namespace UserBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\SecurityContextInterface;
use UserBundle\Entity\User;

class LastActivityListener
{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var SecurityContextInterface
     */
    protected $sc;

    public function __construct(EntityManager $em, SecurityContextInterface $sc)
    {
        $this->em = $em;
        $this->sc = $sc;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->isMasterRequest() && $token = $this->sc->getToken()) {
            /** @var User $user */
            $user = $token->getUser();
            if (is_object($user) && (null == $user->getLastActivity() || $user->getLastActivity() < new \DateTime('-1min'))) {
                $user->setLastActivity(new \DateTime());
                $this->em->flush($user);
            }
        }
    }
}
