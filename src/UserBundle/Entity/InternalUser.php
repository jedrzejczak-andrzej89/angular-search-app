<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table("user_internal_user")
 */
class InternalUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $sap;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $communicator;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $replanishment;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $replanishment_delay;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $automat;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $fit_level;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $fus;


    /**
     * @return int
     */
    public function getSap()
    {
        return $this->sap;
    }

    /**
     * @param int $sap
     */
    public function setSap($sap)
    {
        $this->sap = $sap;
    }

    /**
     * @return int
     */
    public function getCommunicator()
    {
        return $this->communicator;
    }

    /**
     * @param int $communicator
     */
    public function setCommunicator($communicator)
    {
        $this->communicator = $communicator;
    }

    /**
     * @return int
     */
    public function getReplanishment()
    {
        return $this->replanishment;
    }

    /**
     * @param int $replanishment
     */
    public function setReplanishment($replanishment)
    {
        $this->replanishment = $replanishment;
    }

    /**
     * @return int
     */
    public function getReplanishmentDelay()
    {
        return $this->replanishment_delay;
    }

    /**
     * @param int $replanishment_delay
     */
    public function setReplanishmentDelay($replanishment_delay)
    {
        $this->replanishment_delay = $replanishment_delay;
    }

    /**
     * @return mixed
     */
    public function getFitLevel()
    {
        return $this->fit_level;
    }

    /**
     * @param mixed $fit_level
     */
    public function setFitLevel($fit_level)
    {
        $this->fit_level = $fit_level;
    }

    /**
     * @return mixed
     */
    public function getFus()
    {
        return $this->fus;
    }

    /**
     * @param mixed $fus
     */
    public function setFus($fus)
    {
        $this->fus = $fus;
    }

    /**
     * @return int
     */
    public function getAutomat()
    {
        return $this->automat;
    }

    /**
     * @param int $automat
     */
    public function setAutomat($automat)
    {
        $this->automat = $automat;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}