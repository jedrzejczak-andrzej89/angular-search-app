<?php
namespace CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class ViewStrategyCompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('core.view_strategy_pool')) {
            return;
        }

        $definition = $container->getDefinition('core.view_strategy_pool');
        foreach ($container->findTaggedServiceIds('core.view_strategy') as $id => $attributes) {
            $definition->addMethodCall('addViewStrategy', [new Reference($id), $attributes]);
        }
    }
}