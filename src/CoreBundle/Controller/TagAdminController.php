<?php
namespace CoreBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as BaseController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TagAdminController extends BaseController
{
    /**
     * return the Response object associated to the list action
     *
     * @return Response
     */
    public function listAction()
    {
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        $datagrid = $this->admin->getDatagrid();
        if ($tag = $this->getRequest()->query->get('tag')) {
            if ($tag != "Wszystko") {
                $datagrid->setValue('contexts', null, $tag);
            }
        }
        $formView = $datagrid->getForm()->createView();
        $tags = $this->getDoctrine()
            ->getRepository('CoreBundle:TagContext')->findAll();
        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render($this->admin->getTemplate('list'), array(
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'tags' => $tags,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }
}
