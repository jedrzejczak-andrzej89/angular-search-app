<?php

namespace CoreBundle\EventListener;


use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class TemplateListener
{
    private $template;
    private $container;

    public function __construct(TwigEngine $template, Container $container)
    {
        $this->template = $template;
        $this->container = $container;
    }

    public function preExecute(GetResponseForControllerResultEvent $event){
            $theme = $this->container->getParameter('theme');
            $request = $event->getRequest();
            $template = $request->get('_template');
            if ($template && !is_object($template)) {
                $dir = sprintf(':%s/%s', $theme, $template);
                $data = $event->getControllerResult();
                $response = $this->template->renderResponse($dir, $data);
                $event->setResponse($response);
            }
    }
}