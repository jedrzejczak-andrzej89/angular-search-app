<?php

namespace CoreBundle\Block;

use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

abstract class AbstractBlock extends BaseBlockService
{
    private $container;

    public function __construct($name, EngineInterface $templating, ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct($name, $templating);
    }

    /**
     * @return array cacheKeys
     * @see \Sonata\BlockBundle\Block\BaseBlockService::getCacheKeys()
     */
    public function getCacheKeys(BlockInterface $block)
    {
        throw new \BadMethodCallException("Block must implement cache mechanism");
    }

    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(['template' => "CoreBundle:Block:base_block.html.twig"]);
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        return $this->renderResponse($blockContext->getTemplate(),
            ['settings'  => $blockContext->getSettings()], $response)->setTtl(3600);
    }

    public function renderResponse($view, array $parameters = array(), Response $response = null)
    {
        (func_num_args() > 3) ? $cacheTime = func_get_arg(3) : $cacheTime = 0;
        $theme = $this->container->getParameter('theme');
        $view = sprintf(':%s/%s', $theme, $view);

        return $this->getTemplating()->renderResponse($view, $parameters, $response)->setTtl(0);
    }

    public function validateBlock(ErrorElement $errorElement, BlockInterface $block){}

    public function buildEditForm(FormMapper $formMapper, BlockInterface $block){}
}
