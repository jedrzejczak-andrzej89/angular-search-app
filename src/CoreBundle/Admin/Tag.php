<?php
namespace CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class Tag extends Admin
{

    protected $datagridValues = [
        '_page' => 1,
        '_sort_by' => 'name',
        '_sort_order' => 'ASC'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('Ogólne')
            ->add('name', null, ['label' => 'Nazwa'])
            ->add('contexts', null, ['label' => 'Kontekstsy użycia'])
        ->end()
        ->add('slug', null, [
            'required' => false,
            'label' => 'Nazwa w URL',
            'read_only' => true,
            'disabled'  => true
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, ['label' => 'Nazwa'])
            ->add('slug', null, ['label' => 'Nazwa w URL'])
            ->add("contexts", 'doctrine_orm_callback', [ 'label' => 'Konteksty użycia',
                 'callback' => function($queryBuilder, $alias, $field, $value) {
                     if (!$value || !$value['value']) {
                         return false;
                     } else {
                         $queryBuilder->leftJoin(sprintf('%s.contexts', $alias), 'tc');
                         $queryBuilder->andWhere('LOWER(tc.name) = LOWER(:context)');
                         $queryBuilder->setParameter('context', $value['value']);
 
                         return true;
                     }
                }
             ])
         ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, ['label' => 'Nazwa'])
            ->add("contexts", null, ['label' => 'Konteksty użycia'])
        ;
    }
}
