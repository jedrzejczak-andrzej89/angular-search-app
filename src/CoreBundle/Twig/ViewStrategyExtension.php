<?php

namespace CoreBundle\Twig;

use CoreBundle\Service\ViewStrategyPool;

class ViewStrategyExtension extends \Twig_Extension
{
    protected $pool;

    public function __construct(ViewStrategyPool $pool)
    {
        $this->pool = $pool;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_strategy_for', [$this, 'getStrategyFor']),
        ];
    }

    public function getStrategyFor($object, $context = '')
    {
        return $this->pool->getStrategyFor($object, $context);
    }

    public function getName()
    {
        return 'view_strategy_extension';
    }
}