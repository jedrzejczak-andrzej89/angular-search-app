<?php

namespace CoreBundle\Twig;

use CoreBundle\Entity\LinkableInterface;
use Symfony\Component\Routing\RouterInterface;

class RoutingExtension extends \Twig_Extension
{
    protected $container;
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function getFilters()
    {
        return [
            'urlize' => new \Twig_Filter_Method($this, 'getUrl', ['is_safe' => ['html']]),
            'anchor' => new \Twig_Filter_Method($this, 'getAnchor', ['is_safe' => ['html']])
        ];
    }

    public function getUrl($data, $additionalParams = [], $absolute = false)
    {
        if ($data instanceof LinkableInterface) {
            $route = $data->getRoute();
            $params = $data->getRouteParameters();
        } elseif (is_array($data)) {
            $route = isset($data['route']) ? $data['route'] : $data[0];
            $params = isset($data['routeParameters']) ? $data['routeParameters'] : $data[1];
        } else {
            $route = $data;
            $params = [];
        }

        return $this->router->generate($route, $params + $additionalParams, $absolute);
    }

    public function getAnchor($data, $classes = '', $additionalParams = [], $absolute = false)
    {
        $url = $this->getUrl($data, $additionalParams, $absolute);
        $title = $this->getTitle($data);

        if (is_array($classes)) {
            $classes = implode(' ', $classes);
        }

        if ($classes) {
            return sprintf('<a href="%s" class="%s">%s</a>', $url, $classes, $title);
        } else {
            return sprintf('<a href="%s">%s</a>', $url, $title);
        }
    }

    public function getName()
    {
        return 'core_routing_extesion';
    }

    private function getTitle($data)
    {
        if ($data instanceof LinkableInterface) {
            return $data->getPageTitle();
        } elseif (is_array($data)) {
            if (isset($data['title'])) {
                return $data['title'];
            } elseif (isset($data[2])) {
                return $data[2];
            }
        }

        return '';
    }
}