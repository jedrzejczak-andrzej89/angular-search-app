<?php

namespace CoreBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Util\ClassUtils;

class ViewStrategyPool
{
    protected $pool;
    private $resolver;

    public function __construct()
    {
        $this->pool = new ArrayCollection();
        $this->resolver = new OptionsResolver();
        $this->configureTagOptions();
    }

    /** @return ViewStrategyInterface */
    public function getStrategyFor($object, $context = '')
    {
        $key = $this->getPoolPath(ClassUtils::getRealClass($object), $context);
        if(!$this->pool->containsKey($key)) {
            throw new \InvalidArgumentException("No strategy is registered for this object and context");
        }

        return $this->pool[$key]->setObject($object);
    }

    public function addViewStrategy(ViewStrategyInterface $definition, array $tags)
    {
        foreach($tags as $options) {
            $options = $this->resolveTagOptions($options);
            $key = $this->getPoolPath($options['class'], $options['context']);
            if($this->pool->containsKey($key)) {
                throw new \InvalidArgumentException("Strategy for this class and context is already registered");
            }
            $this->pool[$key] = $definition;
        }
    }

    protected function getPoolPath($class, $context)
    {
        return $class . ($context ? "_" . $context : '');
    }

    protected function configureTagOptions()
    {
        $this->resolver
            ->setRequired(['class'])
            ->setAllowedValues([
                'class' => function ($value) {
                    return class_exists($value);
                },
            ])
            ->setDefaults(['context' => '']);
    }

    protected function resolveTagOptions($options)
    {
        return $this->resolver->resolve($options);
    }
}