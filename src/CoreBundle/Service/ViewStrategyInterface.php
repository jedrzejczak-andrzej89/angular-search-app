<?php

namespace CoreBundle\Service;

interface ViewStrategyInterface
{
    /** @return ViewStrategyInterface $this */
    public function setObject($object);
}