<?php

namespace CoreBundle\Entity;

interface LinkableInterface
{
    public function getRoute();

    public function getRouteParameters();
}