<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Tag
 *
 * @ORM\Table(name="core_tag")
 * @ORM\Entity(repositoryClass="TagRepository")
 */
class Tag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     *
     **/
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity="TagContext", inversedBy="tags")
     * @ORM\JoinTable(name="core_tags_contexts")
     **/
    private $contexts;

    public function __construct($name = null)
    {
        $this->setName($name);
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    public function hasContext($name){
        foreach($this->getContexts() as $c){
            if($c->getName() == $name){
                return true;
            }
        }
        return false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Tag
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $contexts
     */
    public function setContexts($contexts)
    {
        foreach($contexts as $context) {
            $this->addContext($context);
        }
    }

    /**
     * Add contexts
     *
     * @param TagContext $contexts
     * @return Tag
     */
    public function addContext(TagContext $contexts)
    {
        $this->contexts[] = $contexts;

        return $this;
    }

    /**
     * Remove contexts
     *
     * @param TagContext $contexts
     */
    public function removeContext(TagContext $contexts)
    {
        $this->contexts->removeElement($contexts);
    }

    /**
     * Get contexts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContexts()
    {
        return $this->contexts;
    }
}