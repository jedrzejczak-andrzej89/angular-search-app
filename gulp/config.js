var gutil = require('gulp-util');

var config = {
    env: {
        dev: 'dev',
        prod: 'prod'
    },
    paths: {
        common: {
            bower: 'bower_components',
            node: 'node_modules'
        },
        src: {
            base: 'app/Resources',
            css: '/css',
            fonts: '/fonts',
            img: '/img',
            js: '/js',
            twig: '/views'
        },
        dest: {
            base: 'web',
            assets: 'web/assets'
        }
    }
};

exports.env = gutil.env.env;

exports.paths = {
    common: config.paths.common,
    src: config.paths.src,
    dest: config.paths.dest
};

exports.errorHandler = function(title) {
    'use strict';

    return function(err) {
        gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
        this.emit('end');
    };
};