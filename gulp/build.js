var gulp = require('gulp'),
    config = require('./config'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    livereload = require('gulp-livereload');

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del']
});

gulp.task('clean', function () {
    return $.del([config.paths.dest.assets]);
});

gulp.task('copy:fonts', ['clean'], function () {
    return gulp.src([
        config.paths.src.base + '/**/*.{eot,svg,ttf,woff,woff2}'
    ])
        .pipe(gulp.dest(config.paths.dest.base))
        .pipe(livereload());
});

gulp.task('copy:images', ['clean'], function () {
    return gulp.src([
        config.paths.src.base + '/**/*.{jpg,jpeg,png,gif,svg}'
    ])
        .pipe(gulp.dest(config.paths.dest.base))
        .pipe(livereload());
});

gulp.task('copy:js', ['clean'], function () {
    return gulp.src([
        config.paths.src.base + '/**/*.js'
    ])
        .pipe(gulp.dest(config.paths.dest.base))
        .pipe(livereload());
});

gulp.task('styles', ['clean'], function () {
    return gulp.src([
        config.paths.src.base + '/**/*.css'
    ])
        .pipe(sourcemaps.init())
        .pipe(gulp.dest(config.paths.dest.base))
        .pipe(livereload());
});

gulp.task('html', function() {
    return gulp.src([
        config.paths.src.base + '/**/*.twig'
    ])
        .pipe(gulp.dest(''))
        .pipe(livereload());
});

gulp.task('watch', function () {
    livereload.listen();

    gulp.watch(config.paths.src.base + '/**/*.fonts', ['copy:fonts']);
    gulp.watch(config.paths.src.base + '/**/*.{jpg,jpeg,png,gif,svg}', ['copy:images']);
    gulp.watch(config.paths.src.base + '/**/*.js', ['copy:js']);
    gulp.watch(config.paths.src.base + '/**/*.css', ['styles']);
    gulp.watch(config.paths.src.base + '/**/*.twig', ['html']);
});

gulp.task('copy', [
    'clean',
    'copy:fonts',
    'copy:images',
    'copy:js'
]);

gulp.task('build', [
    'clean',
    'copy',
    'styles',
    'html',
    'watch'
]);