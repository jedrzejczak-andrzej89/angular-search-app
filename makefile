clean:
	php app/console cache:clear
	php app/console assets:install

assetic:
	php app/console assets:install web

install: do_install clean

do_install:
	mkdir -p app/cache app/logs web/uploads
	touch app/config/allowed_dev_ips
	echo "127.0.0.1\n::1" > app/config/allowed_dev_ips
	rm -rf app/cache/*
	rm -rf app/logs/*
	@if setfacl;\
	then\
		setfacl -R -m u:www-data:rwx -m u:`whoami`:rwx app/cache app/logs web/uploads;\
		setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx app/cache app/logs web/uploads;\
	else\
		HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`;\
		chmod +a "$HTTPDUSER allow delete,write,append,file_inherit,directory_inherit" app/cache app/logs;\
		chmod +a "`whoami` allow delete,write,append,file_inherit,directory_inherit" app/cache app/logs;\
	fi;
	php composer.phar install -o
	-php app/console doctrine:database:create
	-php app/console doctrine:migrations:migrate --no-interaction

update: do_update assetic clean

recreate_db:
	-php app/console doctrine:database:drop --force
	php app/console doctrine:database:create

do_update:
	git pull
	php composer.phar install -o
	php app/console doctrine:migrations:migrate --no-interaction