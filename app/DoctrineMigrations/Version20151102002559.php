<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151102002559 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE core_tags_contexts (tag_id INT NOT NULL, tag_context_id INT NOT NULL, INDEX IDX_4E62B8F3BAD26311 (tag_id), INDEX IDX_4E62B8F3CE1B439E (tag_context_id), PRIMARY KEY(tag_id, tag_context_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE core_tags_contexts ADD CONSTRAINT FK_4E62B8F3BAD26311 FOREIGN KEY (tag_id) REFERENCES core_tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE core_tags_contexts ADD CONSTRAINT FK_4E62B8F3CE1B439E FOREIGN KEY (tag_context_id) REFERENCES core_tagcontext (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX UNIQ_3E2661AFAC51CEB5 ON core_tag');
        $this->addSql('ALTER TABLE core_tag CHANGE contexts slug VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3E2661AF989D9B62 ON core_tag (slug)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE core_tags_contexts');
        $this->addSql('DROP INDEX UNIQ_3E2661AF989D9B62 ON core_tag');
        $this->addSql('ALTER TABLE core_tag CHANGE slug contexts VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3E2661AFAC51CEB5 ON core_tag (contexts)');
    }
}
