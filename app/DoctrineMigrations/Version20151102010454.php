<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151102010454 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cms_cms_tag (cms_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_1C6071E9BE8A7CFB (cms_id), INDEX IDX_1C6071E9BAD26311 (tag_id), PRIMARY KEY(cms_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cms_cms_tag ADD CONSTRAINT FK_1C6071E9BE8A7CFB FOREIGN KEY (cms_id) REFERENCES cms_cms (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cms_cms_tag ADD CONSTRAINT FK_1C6071E9BAD26311 FOREIGN KEY (tag_id) REFERENCES core_tag (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE cms_cms_tag');
    }
}
