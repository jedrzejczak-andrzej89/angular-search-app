<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151101223740 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE news_news ADD gallery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE news_news ADD CONSTRAINT FK_C80E6FDB4E7AF8F FOREIGN KEY (gallery_id) REFERENCES media__gallery (id)');
        $this->addSql('CREATE INDEX IDX_C80E6FDB4E7AF8F ON news_news (gallery_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE news_news DROP FOREIGN KEY FK_C80E6FDB4E7AF8F');
        $this->addSql('DROP INDEX IDX_C80E6FDB4E7AF8F ON news_news');
        $this->addSql('ALTER TABLE news_news DROP gallery_id');
    }
}
