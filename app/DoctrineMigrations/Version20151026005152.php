<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151026005152 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE news_news (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, lead LONGTEXT NOT NULL, content LONGTEXT NOT NULL, published TINYINT(1) NOT NULL, publish_date DATETIME NOT NULL, created DATETIME NOT NULL, UNIQUE INDEX UNIQ_C80E6FDB989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_categories_news (news_id INT NOT NULL, news_category_id INT NOT NULL, INDEX IDX_27C751C5B5A459A0 (news_id), INDEX IDX_27C751C53B732BAD (news_category_id), PRIMARY KEY(news_id, news_category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_news_category (id INT AUTO_INCREMENT NOT NULL, tree_parent_id INT DEFAULT NULL, slug VARCHAR(128) NOT NULL, name VARCHAR(255) NOT NULL, tree_lft INT NOT NULL, tree_lvl INT NOT NULL, tree_rgt INT NOT NULL, tree_root INT DEFAULT NULL, position INT NOT NULL, UNIQUE INDEX UNIQ_1A91D6D6989D9B62 (slug), INDEX IDX_1A91D6D6B5CEAE2F (tree_parent_id), INDEX IDX_1A91D6D6989D9B62 (slug), INDEX IDX_1A91D6D65E237E06 (name), INDEX IDX_1A91D6D68948B361 (tree_lft), INDEX IDX_1A91D6D6D0E63966 (tree_lvl), INDEX IDX_1A91D6D686EB0C5A (tree_rgt), INDEX IDX_1A91D6D6A977936C (tree_root), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE news_categories_news ADD CONSTRAINT FK_27C751C5B5A459A0 FOREIGN KEY (news_id) REFERENCES news_news (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news_categories_news ADD CONSTRAINT FK_27C751C53B732BAD FOREIGN KEY (news_category_id) REFERENCES news_news_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news_news_category ADD CONSTRAINT FK_1A91D6D6B5CEAE2F FOREIGN KEY (tree_parent_id) REFERENCES news_news_category (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE news_categories_news DROP FOREIGN KEY FK_27C751C5B5A459A0');
        $this->addSql('ALTER TABLE news_categories_news DROP FOREIGN KEY FK_27C751C53B732BAD');
        $this->addSql('ALTER TABLE news_news_category DROP FOREIGN KEY FK_1A91D6D6B5CEAE2F');
        $this->addSql('DROP TABLE news_news');
        $this->addSql('DROP TABLE news_categories_news');
        $this->addSql('DROP TABLE news_news_category');
    }
}
