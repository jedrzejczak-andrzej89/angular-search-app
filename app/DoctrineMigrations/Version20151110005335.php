<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151110005335 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO menu_route (name, route) VALUES ('Strona logowania', 'fos_user_security_login')");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
