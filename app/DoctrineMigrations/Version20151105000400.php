<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151105000400 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE menu_menu (id INT AUTO_INCREMENT NOT NULL, tree_parent_id INT DEFAULT NULL, cms_id INT DEFAULT NULL, category_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, tree_lft INT NOT NULL, tree_lvl INT NOT NULL, tree_rgt INT NOT NULL, tree_root INT DEFAULT NULL, position INT NOT NULL, INDEX IDX_B54ACADDB5CEAE2F (tree_parent_id), INDEX IDX_B54ACADDBE8A7CFB (cms_id), INDEX IDX_B54ACADD12469DE2 (category_id), INDEX IDX_B54ACADD5E237E06 (name), INDEX IDX_B54ACADD8948B361 (tree_lft), INDEX IDX_B54ACADDD0E63966 (tree_lvl), INDEX IDX_B54ACADD86EB0C5A (tree_rgt), INDEX IDX_B54ACADDA977936C (tree_root), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE menu_menu ADD CONSTRAINT FK_B54ACADDB5CEAE2F FOREIGN KEY (tree_parent_id) REFERENCES menu_menu (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE menu_menu ADD CONSTRAINT FK_B54ACADDBE8A7CFB FOREIGN KEY (cms_id) REFERENCES cms_cms (id)');
        $this->addSql('ALTER TABLE menu_menu ADD CONSTRAINT FK_B54ACADD12469DE2 FOREIGN KEY (category_id) REFERENCES news_news_category (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE menu_menu DROP FOREIGN KEY FK_B54ACADDB5CEAE2F');
        $this->addSql('DROP TABLE menu_menu');
    }
}
