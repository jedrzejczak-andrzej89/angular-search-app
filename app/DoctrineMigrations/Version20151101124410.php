<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151101124410 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_internal_user (id INT AUTO_INCREMENT NOT NULL, sap INT DEFAULT NULL, communicator INT DEFAULT NULL, replanishment INT DEFAULT NULL, replanishment_delay INT DEFAULT NULL, automat INT DEFAULT NULL, fit_level NUMERIC(10, 6) DEFAULT NULL, fus NUMERIC(10, 6) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_user ADD internal_user_id INT DEFAULT NULL, ADD created_at DATETIME NOT NULL, ADD type TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE user_user ADD CONSTRAINT FK_F7129A80BF7692A3 FOREIGN KEY (internal_user_id) REFERENCES user_internal_user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F7129A80BF7692A3 ON user_user (internal_user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_user DROP FOREIGN KEY FK_F7129A80BF7692A3');
        $this->addSql('DROP TABLE user_internal_user');
        $this->addSql('DROP INDEX UNIQ_F7129A80BF7692A3 ON user_user');
        $this->addSql('ALTER TABLE user_user DROP internal_user_id, DROP created_at, DROP type');
    }
}
