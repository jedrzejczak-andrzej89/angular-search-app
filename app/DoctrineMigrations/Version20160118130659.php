<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160118130659 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cms_cms ADD gallery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cms_cms ADD CONSTRAINT FK_ED8A507C4E7AF8F FOREIGN KEY (gallery_id) REFERENCES media__gallery (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_ED8A507C4E7AF8F ON cms_cms (gallery_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cms_cms DROP FOREIGN KEY FK_ED8A507C4E7AF8F');
        $this->addSql('DROP INDEX UNIQ_ED8A507C4E7AF8F ON cms_cms');
        $this->addSql('ALTER TABLE cms_cms DROP gallery_id');
    }
}
